
import numpy as np
import math
import bisect
import os
import shutil

from scipy.stats import chi2
from itertools import combinations
from sklearn.feature_selection import RFE, RFECV
from classification import get_classification_methods, Classification
from sklearn.covariance import MinCovDet
from sklearn.cross_validation import train_test_split
from sklearn.externals import joblib
import cPickle as pickle

DATASET_TEST_SIZE_DEFAULT_VALUE = 0.2
model_path = os.path.join(os.getcwd(), 'server/models')

def delete_model(array_name):
    folder_path = 'server/models/' + array_name
    for file_object in os.listdir(folder_path):
        file_object_path = os.path.join(folder_path, file_object)
        if os.path.isfile(file_object_path):
            os.unlink(file_object_path)
        else:
            shutil.rmtree(file_object_path)
    os.rmdir(folder_path)

def load_model(model_name):
    with open('server/models/' + model_name + '/model.pkl', 'rb') as read:
        return pickle.load(read)
    

# SUPER CLASS #

class FeatureSelection(object):

    # Superclass: Feature Selection

    def __init__(self, preprocessing):
        self.Preprocessing = preprocessing
        self.score = None

    def get_score(self):
        return self.score

    # Mask a matrix with an array of 1 and 0
    def mask_data_matrix(self, data, mask):
        if (len(data[0]) == len(mask)):
            return [[row[i] for i in range(len(mask)) if mask[i] == 1] for row in data]
        else:
            return 0

    # Splits the dataset + classes into a training set and testing set. Performs training and testing on these datasets.
    def perform_train_test(self, data, classes,
            test_size=DATASET_TEST_SIZE_DEFAULT_VALUE):
        data_train, data_test, classes_train, classes_test = \
                self.tt_split(data, classes, test_size)
        self.training(data_train, classes_train)
        self.score = self.testing(data_test, classes_test)
        return self.score

    # Splits data + classes into a training set and testing set.
    def tt_split(self, data, classes, test_size):
        return train_test_split(data, classes, test_size=test_size)

    def save_model(self, model_name):
        os.makedirs('server/models/' + model_name)
        with open('server/models/' + model_name + '/model.pkl', 'wb') as write:
            pickle.dump(self, write, -1)

###############



# SUB CLASSES #

class FilterSelection(FeatureSelection):

    # Subclass: Feature Selection - Filter approaches

    def __init__(self, estimator, preprocessing):

        # Classification parameters
        estimator_params = EmbeddedSelection.classification_methods[estimator].default_params

        # Classification method
        self.Classifier = EmbeddedSelection.classification_methods[estimator](**estimator_params)

        super(FilterSelection, self).__init__(preprocessing)

    # Returns a masked array of the selected features.
    def get_masked_features(self):
        return self.masked_features

    def get_feature_ranking(self):
        return self.feature_ranking

    def set_feature_ranking(self, feature_ranking):
        self.feature_ranking = feature_ranking

    # Returns a masked array of the selected features.
    def set_masked_features(self, masked_features):
        self.masked_features = masked_features

    def get_nr_of_features(self):
        masked_features = self.masked_features
        return len([x for x in masked_features if x == 1])

    # Reduces data to the selected features and performs prediction using the underlying estimator.
    def training(self, data_train, classes_train):
        self.Classifier = self.Classifier.fit(data_train, classes_train)

    # Reduces data to the selected features and returns the score from the underlying estimator.
    def testing(self, data_test, classes_test):
        return self.Classifier.score(data_test, classes_test)


class WrapperSelection(FeatureSelection):

    # Subclass: Feature Selection - Wrapper approaches

    def __init__(self, estimator, preprocessing):

        # Classification parameters
        estimator_params = EmbeddedSelection.classification_methods[estimator].default_params

        # Classification method
        self.Classifier = EmbeddedSelection.classification_methods[estimator](**estimator_params)

        super(WrapperSelection, self).__init__(preprocessing)

    def objective_function(self):
        pass

    # Returns a masked array of the selected features.
    def get_masked_features(self):
        return self.masked_features

    def get_feature_ranking(self):
        return [1 if m == 1 else 2 for m in self.get_masked_features()]

    # Returns a masked array of the selected features.
    def set_masked_features(self, masked_features):
        self.masked_features = masked_features

    def get_nr_of_features(self):
        masked_features = self.masked_features
        return len([x for x in masked_features if x == 1])

    # Reduces data to the selected features and performs prediction using the underlying estimator.
    def training(self, data_train, classes_train):
        self.Classifier = self.Classifier.fit(data_train, classes_train)

    # Reduces data to the selected features and performs prediction using the underlying estimator.
    def predict(self, data):
        return self.Classifier.predict(data)

    # Reduces data to the selected features and returns the score from the underlying estimator.
    def testing(self, data_test, classes_test):
        return self.Classifier.score(data_test, classes_test)


class EmbeddedSelection(FeatureSelection):
    # Subclass: Feature Selection - Embedded approaches
    classification_methods = get_classification_methods(Classification)

    def __init__(self, selector, preprocessing):

        self.selector = selector

        super(EmbeddedSelection, self).__init__(preprocessing)

    # Returns the model.
    def get_model(self):
        return self.selector

    # Set new model.
    def set_model(self, selector):
        self.selector = selector

    # Returns a masked array of the selected features.
    def get_masked_features(self):
        return map(lambda x: 1 if x else 0, self.selector.get_support(False))

    def get_nr_of_features(self):
        return self.selector.n_features_

    def get_feature_ranking(self):
        return self.selector.ranking_

    # Reduces data to the selected features and performs prediction using the underlying estimator.
    def training(self, data_train, classes_train):
        self.selector = self.selector.fit(data_train, classes_train)

    # Reduces data to the selected features and returns the score from the underlying estimator.
    def testing(self, data_test, classes_test):
        return self.selector.score(data_test, classes_test)

    # Reduces data to the selected features and performs prediction using the underlying estimator.
    def predict(self, data):
        return self.selector.predict(data)

    # Reduces data to the selected features.
    def transform(self, data):
        return self.selector.transform(data)

###############



# SUB-SUB CLASSES #

class RobustFeatureSelection(FilterSelection):

    # Subsubclass: Robust Feature Selection
    def __init__(self, estimator, preprocessing):
        super(RobustFeatureSelection, self).__init__(estimator, preprocessing)

    # Signal-to-noise ratio, eq. 3
    def SNR(self, data, j, m, cov_vec, alpha):
        sigma = math.sqrt(sum((math.pow(data[k][j], 2) / cov_vec[(j * m) + k]) for k in range(m)))
        return (1.0 / chi2.ppf(alpha, m)) * sigma

    # Nordlings confidence score, eq. 2a
    def NCS(self, feature_indexes, j, m, n, data, classes, cov_vec, alpha):
        # Eq. 2c, 2d, 2e
        if j == 'ZERO':
            psi = [[row[f] for f in feature_indexes] for row in data]
        elif j == 'INF':
            psi = [[row[f] for f in feature_indexes] for row in data]
            for row_index in range(len(psi)):
                psi[row_index].append(classes[row_index])
        else:
            psi = [[row[f] for f in feature_indexes if f != j] for row in data]
            for row_index in range(len(psi)):
                psi[row_index].append(classes[row_index])

        # Eq. 2b
        confidence_score = np.zeros((m, (n + 1)))
        chi_inv = chi2.ppf(alpha, (n * m))
        for (k, l), element in np.ndenumerate(psi):
            if j == 'ZERO' or j == 'INF':
                variance = cov_vec[((l - 1) * m) + k]
            elif l <= j:
                variance = cov_vec[((l - 1) * m) + k]
            elif j < l and l <= n:
                variance = cov_vec[(l * m) + k]

            confidence_score[k, l] = psi[k][l] / math.sqrt(chi_inv * variance)
        

        # Singular value
        P, D, Q = np.linalg.svd(confidence_score, full_matrices=False)

        return np.min(D[np.nonzero(D)])


    def robust_selection(self, data, data_errors, classes, classes_errors,
            test_size=DATASET_TEST_SIZE_DEFAULT_VALUE):
        
        data_train, data_test, classes_train, classes_test = \
                self.tt_split(data, classes, test_size)

        feature_indexes = [int(i) for i in range(len(data[0]))] # Feature index list
        feature_ranking = [0 for fi in feature_indexes]
        current_ranking = len(feature_indexes)
        m = len(data) # Row count of dataset
        n = len(data[0]) # Column count of dataset
        alpha = 0.05 # Significance level

        # Calculation of covariance vector
        data_variance_per_column = np.var(data_errors, axis=0).tolist()
        classes_variance_per_column = np.var(classes_errors, axis=0).tolist()

        cov_vec = np.zeros(m*(n+1))

        for (k, j), element in np.ndenumerate(data):
            cov_vec[((j * m) + k)] = data_variance_per_column[j]

        for k in range(len(classes)):
            cov_vec[((n * m) + k)] = classes_variance_per_column

        # Calculating SNR
        feature_indexes_snr = np.array(
                [(j, self.SNR(data, j, m, cov_vec, alpha))
                    for j in feature_indexes])
        feature_indexes_snr = feature_indexes_snr[np.argsort(feature_indexes_snr[:,1])]

        # Step 2: If m < n, then remove the n - m features with smallest SNR.
        if m < n:
            for remove in range(n - m):
                feature_indexes.remove(int(feature_indexes_snr[0][0]))
                feature_ranking[int(feature_indexes_snr[0][0])] = current_ranking
                current_ranking -= 1
                feature_indexes_snr = np.delete(feature_indexes_snr, 0, axis=0)

        # Step 3: Nordlings confidence score = NCS, while NCS(0) < 1 and
        #NCS(inf) < 1 for feature_indexes without the feature
        #with smallest SNR, remove it.
        while len(feature_indexes) > 1:
            feature_indexes.remove(int(feature_indexes_snr[0][0]))

            ncs_zero = self.NCS(feature_indexes, 'ZERO', m, n, data, classes, cov_vec, alpha)
            ncs_inf = self.NCS(feature_indexes, 'INF', m, n, data, classes, cov_vec, alpha)

            if ncs_zero >= 1.0 or ncs_inf >= 1.0:
                bisect.insort_left(feature_indexes, int(feature_indexes_snr[0][0]))
                break
            else:
                feature_ranking[int(feature_indexes_snr[0][0])] = current_ranking
                current_ranking -= 1
                feature_indexes_snr = np.delete(feature_indexes_snr, 0, axis=0)

        # Step 4: Nordlings confidence score = NCS, calculate NCS(j) for all j and keep those with score above one
        ncs_j = np.array(
            [(j,
            self.NCS(feature_indexes, j, m, n, data, classes, cov_vec, alpha))
            for j in feature_indexes])
        masked_array = [0] * n

        some_value = False
        for x in ncs_j:
            if x[1] > 1.0:
                some_value = True
                masked_array[int(x[0])] = 1

        if some_value == False:
            for x in ncs_j:
                masked_array[int(x[0])] = 1

        ncs_j = ncs_j[np.argsort(ncs_j[:,1])]

        for x in ncs_j:
            feature_ranking[int(x[0])] = current_ranking
            current_ranking -= 1

        self.set_feature_ranking(feature_ranking)
        self.set_masked_features(masked_array)

        return self.perform_train_test(self.mask_data_matrix(data, masked_array), classes, test_size)


    # Splits data + classes into a training set and testing set.
    def tt_split(self, data, classes, test_size):
        return train_test_split(data, classes, test_size=test_size)


class ExhaustiveFeatureSelection(WrapperSelection):

    # Subsubclass: Exhaustive Feature Selection

    MAX_FEATURE_COUNT = 10

    def __init__(self, estimator, preprocessing, params = {'min_set_size': 2, 'max_set_size': 5}):

        self.min_set = params['min_set_size']
        self.max_set = params['max_set_size']
        
        super(ExhaustiveFeatureSelection, self).__init__(estimator, preprocessing)

    def exhaustive_search(self, data, classes, test_size=DATASET_TEST_SIZE_DEFAULT_VALUE):

        score = None
        feature_score = []
        max_set = self.max_set
        min_set = self.min_set

        # If the dataset contains of less features than max size of subset the max set is set to dataset size.
        if(len(data[0]) < max_set):
            max_set = len(data[0]) - 1

        # Create local index of dataset. [0:N features]
        index_array = np.arange(0, len(data[0]))

        # Generate all possible subset of features between minimum and maximum size.
        for L in range(min_set, max_set):
            for subset in combinations(index_array, L):
                subset = list(subset)
                score = self.objective_function(data[:, subset], classes, test_size)
                feature_score.append((score, ','.join(str(x) for x in subset)))

        def comparison(a, b):
            if (a[0] > b[0]):
                return 1
            elif (a[0] == b[0]):
                if (len(a[1]) < len(b[1])):
                    return 1
                else:
                    return -1
            else:
                return -1

        # Sort list of features according to their score from objective function
        feature_score = sorted(feature_score, reverse=True, cmp=comparison)
        selected_features = [int(x) for x in feature_score[0][1].split(',')]
        masked_array = [0]*len(index_array)

        for x in selected_features:
            masked_array[x] = 1 

        self.set_masked_features(masked_array)

        return self.perform_train_test(self.mask_data_matrix(data, masked_array), classes, test_size)


    def objective_function(self, data, classes, test_size):
        return self.perform_train_test(data, classes, test_size)


class RecursiveFeatureElimination(EmbeddedSelection):

    # Subsubclass: Recursive Feature Elimination

    def __init__(self, estimator, preprocessing, params = {'n_features_to_select': 5}):

        # Classification parameters
        estimator_params = EmbeddedSelection.classification_methods[estimator].default_params

        # Classification method
        self.Classifier = EmbeddedSelection.classification_methods[estimator](**estimator_params)

        # FS parameters
        fs_params = params

        # FS method
        super(RecursiveFeatureElimination, self).__init__(RFE(estimator=self.Classifier, **fs_params), preprocessing)


class RecursiveFeatureEliminationCrossValidation(EmbeddedSelection):

    # Subsubclass: Recursive Feature Elimination

    default_params = {
        'scoring': 'accuracy'
    }

    def __init__(self, estimator, preprocessing):

        # Classification parameters
        estimator_params = EmbeddedSelection.classification_methods[estimator].default_params

        # Classification method
        self.Classifier = EmbeddedSelection.classification_methods[estimator](**estimator_params)

        # FS parameters
        fs_params = self.default_params

        # FS method
        super(RecursiveFeatureEliminationCrossValidation, self).__init__(RFECV(estimator=self.Classifier, **fs_params), preprocessing)

